//
//  AxisCollection.swift
//  MetalChartRenderer
//
//  Created by Алексей Остапенко on 14/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

class AxisView: UIView {
    
    enum Direction {
        case horizontal
        case vertical
    }
    
    var direction: Direction = .horizontal
    
    var converter: (Any) -> String = { smt in
        return ""
    }
    
    var items = [Any]() {
        didSet {
            guard items.count > 0 else { return }
            
            let step = Float(items.count - 1) / Float(maxItemOnScreen - 1)
            
            for i in 0..<maxItemOnScreen {
                labels[i].text = converter(items[Int(step * Float(i))])
            }
        }
    }
    
    var visibleRange: Range<CGFloat>? {
        willSet {
            guard let newVisibleRange = newValue else { return }
            
            let intLower = Int(newVisibleRange.lowerBound)
            let intUpper = Int(newVisibleRange.upperBound)
            
            let count = intUpper - intLower
            let step = Float(count) / Float(maxItemOnScreen - 1)
        
            for i in 0..<maxItemOnScreen {
                let index = intLower + Int((step) * Float(i))
                labels[i].text = converter(items[index])
            }
        }
    }
    
    var maxItemOnScreen = 5
    
    var labels = [UILabel]()
    
    var animatebleView: UIView?
    var animatebleViewMovePoint: CGFloat?
    
    convenience init(frame: CGRect, direction: Direction, itemSize: CGSize, space: CGFloat, maxItemOnScreen: Int = 5) {
        self.init(frame: frame)
        
        self.maxItemOnScreen = maxItemOnScreen
        self.direction = direction
        
        for i in 0..<maxItemOnScreen {
            let label = UILabel(frame: CGRect(origin: .zero, size: itemSize))
            label.frame.origin = direction == .horizontal ? CGPoint(x: CGFloat(i) * itemSize.width + CGFloat(i) * space, y: 0) : CGPoint(x: 0, y: CGFloat(i) * itemSize.width + CGFloat(i) * space)
            label.font = UIFont.systemFont(ofSize: 10)
            label.textAlignment = .center
            label.textColor = .black
            addSubview(label)
            labels.append(label)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let space = direction == .horizontal ? (bounds.width - CGFloat(maxItemOnScreen) * 40) / (CGFloat(maxItemOnScreen) - 1) : (bounds.height - CGFloat(maxItemOnScreen) * 30) / (CGFloat(maxItemOnScreen) - 1)
        for i in labels.indices {
            labels[i].frame.origin = direction == .horizontal ? CGPoint(x: CGFloat(i) * labels[i].frame.width + CGFloat(i) * space, y: 0) : CGPoint(x: 0, y: CGFloat(i) * labels[i].frame.height + CGFloat(i) * space)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
