
//
//  ChartView.swift
//  MetalChartRenderer
//
//  Created by Алексей Остапенко on 15/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit
import MetalKit

class ChartView: UIView {
    
    // MARK: - Property
    
    private(set) var extraInfoLineView: UIView?
    private(set) var extraInfoView: ExtraChartInfoView?
    private(set)  var metalView: MTKView!
    private(set) var xAxisView: AxisView!
    private(set) var yAxisView: AxisView!
    private(set) var chartRenderer: MetalChartRenderer!
    private(set) var horizontalLines = [UIView]()
    private(set) var chart: Chart?
    private(set) var notVisibleCharts = [Int]()
    
    var labelsColor: UIColor = .black {
        didSet {
            xAxisView?.labels.forEach { $0.textColor = labelsColor }
            yAxisView?.labels.forEach { $0.textColor = labelsColor }
        }
    }
    
    var linesColor: UIColor = .black {
        didSet {
            horizontalLines.forEach { $0.backgroundColor = linesColor }
            
        }
    }
    
    var extraInfoViewColor: UIColor = .black {
        didSet {
            extraInfoLineView?.backgroundColor = linesColor
            extraInfoView?.backgroundColor = linesColor
        }
    }
    
    // MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addXAxis()
        addYAxis()
        addHorizontalLines()
        addMetalView()
//        addExtraInfoView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - 
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        
        metalView.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        chartRenderer.setSize(size: metalView.frame.size)
    }
    
    // MARK: -
    
    private func addHorizontalLines() {
        for i in 0..<yAxisView.maxItemOnScreen {
            let line = UIView()
            line.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
            horizontalLines.append(line)
            
            line.translatesAutoresizingMaskIntoConstraints = false
            insertSubview(line, belowSubview: xAxisView)
            
            line.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            line.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            line.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            line.bottomAnchor.constraint(equalTo: yAxisView.labels[i].bottomAnchor).isActive = true
        }
    }
    
    private func addExtraInfoView() {
        let lineView = UIView(frame: CGRect(x: 0, y: 0, width: 1.5, height: metalView.frame.height))
        lineView.backgroundColor = extraInfoViewColor
        metalView.addSubview(lineView)
        lineView.isHidden = true
        extraInfoLineView = lineView
        
        let extraView: ExtraChartInfoView = ExtraChartInfoView.fromNib()
        extraView.backgroundColor = extraInfoViewColor
        extraView.isHidden = true
        extraView.translatesAutoresizingMaskIntoConstraints = false
        metalView.addSubview(extraView)
        
        let centerXAnchor = extraView.centerXAnchor.constraint(equalTo: lineView.centerXAnchor)
        centerXAnchor.priority = .defaultHigh
        centerXAnchor.isActive = true
        extraView.leadingAnchor.constraint(greaterThanOrEqualTo: metalView.leadingAnchor).isActive = true
        extraView.trailingAnchor.constraint(lessThanOrEqualTo: metalView.trailingAnchor).isActive = true
        extraView.topAnchor.constraint(equalTo: lineView.topAnchor).isActive = true
        
        extraInfoView?.removeFromSuperview()
        extraInfoView = extraView
    }
    
    private func addYAxis() {
        let ySpaceWidth: CGFloat = 5
        let xAxisItemSize = CGSize(width: 40, height: 30)
        yAxisView = AxisView(frame: CGRect(x: 0, y: 13, width: 40, height: bounds.height - xAxisView.frame.height - 10), direction: .vertical, itemSize: xAxisItemSize, space: ySpaceWidth, maxItemOnScreen: 6)
        yAxisView.isUserInteractionEnabled = false
        yAxisView.converter = { smt in
            guard let value = smt as? Int else { return "" }
            
            let convertedValue = value > 1000 ? "\(value / 1000)K" : "\(value)"
            return convertedValue
        }
        yAxisView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(yAxisView)
        
        yAxisView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        yAxisView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        yAxisView.bottomAnchor.constraint(equalTo: xAxisView.topAnchor).isActive = true
        yAxisView.widthAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    private func addXAxis() {
        let xAxisItemSize = CGSize(width: 40, height: 30)
        let spaceWidth = (bounds.width - 6 * 40) / 4
        xAxisView = AxisView(frame: CGRect(x: 40, y: bounds.height - 30, width: bounds.width - 40, height: 30), direction: .horizontal, itemSize: xAxisItemSize, space: spaceWidth)
        xAxisView.converter = { smt in
            guard let time = smt as? TimeInterval else { return "" }
            
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "d MMM"
            
            return dateFormater.string(from: Date(timeIntervalSince1970: time / 1000))
        }
        xAxisView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(xAxisView)
        
        xAxisView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
        xAxisView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        xAxisView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 40).isActive = true
        xAxisView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    private func addMetalView() {
        metalView = MTKView()
        metalView.device = MTLCreateSystemDefaultDevice()
        metalView.backgroundColor = .clear
        metalView.clearColor = .init(red: 0, green: 0, blue: 0, alpha: 0)
        let metalChartRender = MetalChartRenderer(withMetalView: metalView)
        metalChartRender.mtkView(metalView, drawableSizeWillChange: metalView.frame.size)
        chartRenderer = metalChartRender
        metalView.delegate = metalChartRender
        
        metalView.translatesAutoresizingMaskIntoConstraints = false
        
        insertSubview(metalView, belowSubview: yAxisView)
        
        metalView.bottomAnchor.constraint(equalTo: xAxisView.topAnchor, constant: 0).isActive = true
        metalView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
        metalView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        metalView.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapClick(_:)))
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(tapClick(_:)))
        metalView.addGestureRecognizer(tapRecognizer)
        metalView.addGestureRecognizer(panRecognizer)
    }
    
    private func showExtraInfo(yValueIndex: Int) {
        guard let chart = chart, let extraView = extraInfoView, yValueIndex >= 0 && yValueIndex < chart.xValues.count else { return }

        var stringValues = [String]()
        var colors = [UIColor]()
        
        for i in chart.yValues.indices where !notVisibleCharts.contains(i) {
            let time = chart.yValues[i][yValueIndex]
            let converted = time > 1000 ? "\(time / 1000)K" : "\(time)"
            stringValues.append(converted)
            colors.append(chart.colors[i])
            
        }
        extraView.set(time: chart.xValues[yValueIndex], values: stringValues, colors: colors)
    }
    
    private func updateYAxis() {
        let yStep =  Int(chartRenderer!.maxY * 1) / (horizontalLines.count)
        var yItems = [Int]()
        var y: Int = 0
        for _ in 0..<horizontalLines.count {
            yItems.append(y)
            y += yStep
        }
        yAxisView.items = yItems.reversed()
    }
    
    private func hideExtraInfoView() {
        extraInfoLineView?.isHidden = true
        extraInfoView?.isHidden = true
    }
    
    // MARK: - IBAction
    
    @IBAction func tapClick(_ gesture: UIPanGestureRecognizer) {
        if extraInfoLineView ==  nil {
            addExtraInfoView()
        }
        
        let point = gesture.location(in: metalView)
        let renderer = chartRenderer!
        let convertedX = renderer.convertToLocalCoordinates(x: point.x) - 0.75
        let index = renderer.index(atX: point.x)
        
        extraInfoLineView?.frame.origin = CGPoint(x: convertedX, y: 0)
        extraInfoLineView?.isHidden = false
        extraInfoView?.isHidden = false
        
        showExtraInfo(yValueIndex: index)
    }
    
    // MARK: -
    
    func renderChart(chart: Chart) {
        self.chart = chart
        xAxisView.items = chart.xValues
        xAxisView.visibleRange = 0..<CGFloat(chart.xValues.count - 1)
        
        chartRenderer.renderChart(chart: chart)
        
        updateYAxis()
    }
    
    func chartAtIndex(index: Int, visible: Bool) {
        hideExtraInfoView()
        
        chartRenderer.chartAtIndex(index: index, visible: visible)
        
        updateYAxis()
        
        if visible {
            if let containerIndex = notVisibleCharts.firstIndex(of: index) {
                notVisibleCharts.remove(at: containerIndex)
            }
        } else {
            notVisibleCharts.append(index)
        }
    }
    
}

// MARK: - ChartWithSlidersDelegate

extension ChartView: ChartWithSlidersDelegate {
    func chartWithSliders(_ slider: ChartWithSliders, lowerBound: CGFloat, upperBound: CGFloat) {
        guard upperBound > lowerBound else { return }
        
        hideExtraInfoView()
        
        xAxisView.visibleRange = lowerBound..<upperBound
        chartRenderer.setVisibleRange(lowerBound: Float(lowerBound), upperBound: Float(upperBound), animated: true)
        
        updateYAxis()
    }
}
