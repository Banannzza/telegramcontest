//
//  ExtraChartInfoView.swift
//  TelegramChart
//
//  Created by Алексей Остапенко on 20/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit



class ExtraChartInfoView: UIView {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var valueStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
    
    func set(time: TimeInterval, values: [String], colors: [UIColor]) {
        
        let stackViewSubviews = valueStackView.arrangedSubviews
        for subview in stackViewSubviews {
            valueStackView.removeArrangedSubview(subview)
            subview.removeFromSuperview()
        }
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MMM d yyyy"
        
        let dateString = dateFormater.string(from: Date(timeIntervalSince1970: time / 1000))
        dateLabel.text = dateString
        
        for i in values.indices {
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 12)
            label.textColor = colors[i]
            label.text = values[i]
            valueStackView.addArrangedSubview(label)
        }
    }
}
