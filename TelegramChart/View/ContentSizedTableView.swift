//
//  ContentSizedTableView.swift
//  TelegramChart
//
//  Created by Алексей Остапенко on 20/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

final class ContentSizedTableView: UITableView {
    override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
