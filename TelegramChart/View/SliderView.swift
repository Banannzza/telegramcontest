//
//  ChartSliderView.swift
//  TelegramQuest
//
//  Created by Алексей Остапенко on 09/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

protocol SliderViewDelegate: AnyObject {
    func chartWithSliders(_ slider: SliderView, leftSliderXPosition: CGFloat, rightSliderXPosition: CGFloat)
}

class SliderView: UIView {
    
    private(set) var leftSlider: UIImageView!
    private(set) var rightSlider: UIImageView!
    private var topBorderView: UIView!
    private var bottomBorderView: UIView!
    private var leftUnselectedView: UIView!
    private var rightUnselectedView: UIView!
    private var selectedView: UIView!
    
    weak var delegate: SliderViewDelegate?
    
    var selectedAreaColor: UIColor? {
        didSet {
            leftSlider?.backgroundColor = selectedAreaColor
            rightSlider?.backgroundColor = selectedAreaColor
            topBorderView?.backgroundColor = selectedAreaColor
            bottomBorderView?.backgroundColor = selectedAreaColor
        }
    }
    
    var unselectedAreaColor: UIColor? {
        didSet {
            leftUnselectedView?.backgroundColor = unselectedAreaColor
            rightUnselectedView?.backgroundColor = unselectedAreaColor
        }
    }
    
    lazy var halfSliderWidth: CGFloat = {
        return leftSlider.frame.width / 2
    }()

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        leftSlider = makeSlider()
//        leftSlider.image = #imageLiteral(resourceName: "leftSliderArrow")
        let leftSliderGesture = UIPanGestureRecognizer(target: self, action: #selector(leftSliderGestureRecognizer(_:)))
        leftSlider.addGestureRecognizer(leftSliderGesture)
        addSubview(leftSlider)
        
        rightSlider = makeSlider()
//        rightSlider.image = #imageLiteral(resourceName: "rightSliderArrow")
        let rightSliderGesture = UIPanGestureRecognizer(target: self, action: #selector(rightSliderGestureRecognizer(_:)))
        rightSlider.addGestureRecognizer(rightSliderGesture)
        rightSlider.frame.origin = CGPoint(x: frame.width - rightSlider.frame.width, y: 0)
        addSubview(rightSlider)
        
        topBorderView = UIView()
        topBorderView.backgroundColor = leftSlider.backgroundColor
        topBorderView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(topBorderView)
        
        topBorderView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topBorderView.leadingAnchor.constraint(equalTo: leftSlider.trailingAnchor).isActive = true
        topBorderView.trailingAnchor.constraint(equalTo: rightSlider.leadingAnchor).isActive = true
        topBorderView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        bottomBorderView = UIView()
        bottomBorderView.backgroundColor = leftSlider.backgroundColor
        bottomBorderView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bottomBorderView)
        
        bottomBorderView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomBorderView.leadingAnchor.constraint(equalTo: leftSlider.trailingAnchor).isActive = true
        bottomBorderView.trailingAnchor.constraint(equalTo: rightSlider.leadingAnchor).isActive = true
        bottomBorderView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        
        leftUnselectedView = UIView()
        leftUnselectedView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(leftUnselectedView)
        
        leftUnselectedView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        leftUnselectedView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        leftUnselectedView.trailingAnchor.constraint(equalTo: leftSlider.leadingAnchor).isActive = true
        leftUnselectedView.heightAnchor.constraint(equalToConstant: frame.height).isActive = true
        
        rightUnselectedView = UIView()
        rightUnselectedView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(rightUnselectedView)
        
        rightUnselectedView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        rightUnselectedView.leadingAnchor.constraint(equalTo: rightSlider.trailingAnchor).isActive = true
        rightUnselectedView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        rightUnselectedView.heightAnchor.constraint(equalToConstant: frame.height).isActive = true
        
        selectedView = UIView()
        let selectedViewGesture = UIPanGestureRecognizer(target: self, action: #selector(selectedViewGestureRecognizer(_:)))
        selectedView.addGestureRecognizer(selectedViewGesture)
        selectedView.isUserInteractionEnabled = true
        selectedView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(selectedView)
        
        selectedView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        selectedView.leadingAnchor.constraint(equalTo: leftSlider.trailingAnchor).isActive = true
        selectedView.trailingAnchor.constraint(equalTo: rightSlider.leadingAnchor).isActive = true
        selectedView.heightAnchor.constraint(equalToConstant: frame.height).isActive = true
    }
    
    @IBAction func selectedViewGestureRecognizer(_ sender: UIPanGestureRecognizer) {
        defer {
            sender.setTranslation(CGPoint.zero, in: selectedView)
        }
        let translation = sender.translation(in: selectedView)
        if rightSlider.center.x + translation.x > frame.width - halfSliderWidth {
            return
        }
        if leftSlider.center.x + translation.x < halfSliderWidth {
            return
        }
        rightSlider.center.x = rightSlider.center.x + translation.x
        leftSlider.center.x = leftSlider.center.x + translation.x
        delegate?.chartWithSliders(self, leftSliderXPosition: leftSlider.frame.origin.x, rightSliderXPosition: rightSlider.frame.origin.x + rightSlider.frame.width)
    }
    
    @IBAction func leftSliderGestureRecognizer(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: leftSlider)
        leftSlider.center.x = max(min(rightSlider.frame.origin.x - halfSliderWidth, leftSlider.center.x + translation.x), halfSliderWidth)
        sender.setTranslation(CGPoint.zero, in: leftSlider)
        delegate?.chartWithSliders(self, leftSliderXPosition: leftSlider.frame.origin.x, rightSliderXPosition: rightSlider.frame.origin.x + rightSlider.frame.width)
    }
    
    @IBAction func rightSliderGestureRecognizer(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: rightSlider)
        rightSlider.center.x = min(max(leftSlider.frame.origin.x + leftSlider.frame.width + halfSliderWidth, rightSlider.center.x + translation.x), frame.width - halfSliderWidth)
        sender.setTranslation(CGPoint.zero, in: rightSlider)
        delegate?.chartWithSliders(self, leftSliderXPosition: leftSlider.frame.origin.x, rightSliderXPosition: rightSlider.frame.origin.x + rightSlider.frame.width)
    }
    
    func update() {
        rightSlider.frame.origin = CGPoint(x: frame.width - rightSlider.frame.width, y: 0)
    }
    
    private func makeSlider() -> UIImageView {
        let slider = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: 20, height: frame.height)))
        slider.backgroundColor = .red
        slider.tintColor = .white
        slider.contentMode = .scaleAspectFit
        slider.isUserInteractionEnabled = true
        return slider
    }
}
