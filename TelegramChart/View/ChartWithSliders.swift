//
//  NewChartWithSliders.swift
//  MetalChartRenderer
//
//  Created by Алексей Остапенко on 15/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import Foundation
import UIKit
import MetalKit

protocol ChartWithSlidersDelegate: AnyObject {
    func chartWithSliders(_ slider: ChartWithSliders, lowerBound: CGFloat, upperBound: CGFloat)
}

class ChartWithSliders: UIView {
    
    private(set) var chartRenderer: MetalChartRenderer!
    var slidersView: SliderView!
    var metalView: MTKView!
    var chartSize: CGSize
    weak var delegate: ChartWithSlidersDelegate?
    var xCount: CGFloat = 0
    
    override init(frame: CGRect) {
        chartSize = CGSize(width: frame.width - 4, height: frame.height)
        
        super.init(frame: frame)
        
        metalView = MTKView()
        metalView.device = MTLCreateSystemDefaultDevice()
        metalView.backgroundColor = .clear
        metalView.clearColor = .init(red: 0, green: 0, blue: 0, alpha: 0)
        let metalChartRender = MetalChartRenderer(withMetalView: metalView)
        metalChartRender.mtkView(metalView, drawableSizeWillChange: metalView.frame.size)
        chartRenderer = metalChartRender
        metalView.delegate = metalChartRender

        addSubviewWithConstraints(metalView)
        
        slidersView = SliderView(frame: bounds)
        slidersView.delegate = self

        addSubviewWithConstraints(slidersView)
        
        slidersView.selectedAreaColor = UIColor.lightGray.withAlphaComponent(0.7)
        slidersView.unselectedAreaColor = UIColor.lightGray.withAlphaComponent(0.3)
        print(xStep)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        chartRenderer.setSize(size: metalView.frame.size)
        slidersView.rightSlider.frame.origin = CGPoint(x: slidersView.frame.width - slidersView.rightSlider.frame.width, y: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - ChartRenderer

extension ChartWithSliders: ChartRenderer {
    var xStep: CGFloat {
        return chartRenderer.xStep
    }
    
    func renderChart(chart: Chart) {
        xCount = CGFloat(chart.xValues.count - 1)
        chartRenderer.renderChart(chart: chart)
    }
}

extension ChartWithSliders: SliderViewDelegate {
    func chartWithSliders(_ slider: SliderView, leftSliderXPosition: CGFloat, rightSliderXPosition: CGFloat) {
        delegate?.chartWithSliders(self, lowerBound: leftSliderXPosition / xStep, upperBound: min((rightSliderXPosition) / xStep, xCount))
    }
}
