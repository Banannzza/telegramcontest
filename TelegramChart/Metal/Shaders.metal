
#include <metal_stdlib>
#include <simd/simd.h>

using namespace metal;

#import "ShaderTypes.h"

typedef struct
{
    float4 clipSpacePosition [[position]];

} RasterizerData;


vertex RasterizerData
vertexShader(uint vertexID [[vertex_id]],
             constant  Vertex *vertices [[buffer(0)]],
             constant float2 *viewportSizePointer [[buffer(1)]],
             constant float2 *scaleFactor [[buffer(2)]],
             constant float2 *moveFactor [[buffer(3)]])
{
    RasterizerData out;
    
    out.clipSpacePosition = vector_float4(0.0, 0.0, 0.0, 1.0);

    float2 pixelSpacePosition = vertices[int(vertexID)].position.xy;
    float2 viewportSize = float2(*viewportSizePointer);
    float2 halfViewportSize = viewportSize / 2.0;
    
    out.clipSpacePosition.xy = (pixelSpacePosition * *scaleFactor + *moveFactor - halfViewportSize) / halfViewportSize;

    return out;
}


fragment float4 fragmentShader(constant float4 *color [[buffer(0)]])
{
    return *color;
}

