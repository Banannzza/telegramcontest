//
//  MetalChartRenderer.swift
//  MetalChartRenderer
//
//  Created by Алексей Остапенко on 16/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import Foundation
import Metal
import MetalKit


class MetalChartRenderer: NSObject, ChartRenderer {
    
    // MARK: - Property

    private let device: MTLDevice
    private let pipelineState: MTLRenderPipelineState
    private let commandQueue: MTLCommandQueue
    
    private let dynamicFrameRateEnabled = true
    private(set) var viewportSize: float2
    
    var contentWidth: Float {
        return Float(viewportSize.x - 10)
    }
    
    private(set) var animators = [ChartAnimator]()
    private(set) var chartVerticiesBuffer = [MTLBuffer]()
    private(set) var colors = [float4]()
    private(set) var primitiveType = MTLPrimitiveType.lineStrip
    
    private(set) var chart: Chart?
    private(set) var xStep: CGFloat = 0
    private(set) var maxY: Float = 0
    private(set) var visibleRange: ClosedRange<Float>?
    private(set) weak var metalView: MTKView?
    
    // MARK: - Init
    
    init(withMetalView metalView: MTKView) {
        self.metalView = metalView
        device = metalView.device!
        let defaultLibrary = device.makeDefaultLibrary()!
        
        let vertexFunction = defaultLibrary.makeFunction(name: "vertexShader")
        let fragmentFunction = defaultLibrary.makeFunction(name: "fragmentShader")
        
        let pipelineStateDescriptor = MTLRenderPipelineDescriptor()
        pipelineStateDescriptor.label = "Chart Pipeline"
        pipelineStateDescriptor.vertexFunction = vertexFunction
        pipelineStateDescriptor.fragmentFunction = fragmentFunction
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = metalView.colorPixelFormat
        pipelineStateDescriptor.supportIndirectCommandBuffers = true
        
        pipelineState = try! device.makeRenderPipelineState(descriptor: pipelineStateDescriptor)
        
        commandQueue = device.makeCommandQueue()!
        
        viewportSize = float2()
    }
    
    // MARK: - ChartRenderer
    
    func renderChart(chart: Chart) {
        animators = []
        chartVerticiesBuffer = []
        colors = []
        
        self.chart = chart
        
        let floatXStep = contentWidth / Float(chart.xValues.count - 1)
        xStep = CGFloat(floatXStep)
        
        maxY = chart.maxYValues.max()!
        let yConverter = Float(viewportSize.y) / (maxY)
        
        let commandBuffer = commandQueue.makeCommandBuffer()!
        let blitEncoder = commandBuffer.makeBlitCommandEncoder()!
        
        for i in chart.yValues.indices {
            var red: CGFloat = 0.0
            var green: CGFloat = 0.0
            var blue: CGFloat = 0.0
            var alpha: CGFloat = 0.0
            let uiColor = chart.colors[i]
            uiColor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
            let color: float4 = [Float(red), Float(green), Float(blue), Float(alpha)]
            colors.append(color)
            
            let chartVerticies = producePathLine(index: i, yValues: chart.yValues[i], xStep: floatXStep, yConverter: yConverter)
            let sharedBuffer = device.makeBuffer(bytes: chartVerticies, length: MemoryLayout<Vertex>.stride * chartVerticies.count, options: .storageModeShared)!
            let privateBuffer = device.makeBuffer(length: sharedBuffer.length, options: .storageModePrivate)!

            blitEncoder.copy(from: sharedBuffer, sourceOffset: 0, to: privateBuffer, destinationOffset: 0, size: sharedBuffer.length)

            chartVerticiesBuffer.append(privateBuffer)
            animators.append(ChartAnimator(xStep: floatXStep, maxY: maxY, viewHeight: Float(viewportSize.y)))
            animators.last?.delegate = self
        }
        
        blitEncoder.endEncoding()
        commandBuffer.commit()
        
        visibleRange = 0...Float(chart.xValues.count - 1)
    }
    
    // MARK: -
    
    func producePathLine(index: Int, yValues: [Float], xStep: Float, yConverter: Float) -> [Vertex] {
        var chartVerticies = [Vertex]()
        
        var x: Float = 0
        
        for i in yValues.indices {
            let y = yConverter * yValues[i]
            chartVerticies.append(Vertex(position: [x, y]))
            x += xStep
        }
        
        return chartVerticies
    }
    
//    func producePathTriangles(xRange: Range<Int>, yValues: [Int], xStep: Float, maxY: Float, color: float4, width: Float) -> [Vertex] {
//        var result = [Vertex]()
//        
//        var index = 0
//        var x: Float = 0
//        
//        var prevC: float2?
//        var prevD: float2?
//        var prevPoint = float2([x, Float(yValues[index]) / maxY * (Float(viewportSize.y - 20))])
//        
//        x += xStep
//        index += 1
//        
//        repeat {
//            let y1 = Float(yValues[index]) / maxY * (Float(viewportSize.y - 20))
//            let curPoint = float2([x, y1])
//            
//            x += xStep
//            index += 1
//            
//            let dir = curPoint - prevPoint
//            let perp = float2([-dir.y, dir.x])
//            let perpLength = sqrtf(perp.x * perp.x + perp.y * perp.y)
//            let perpNormal = float2([perp.x / perpLength, perp.y / perpLength])
//            
//            let A = prevC ?? (prevPoint + perpNormal * width / 2)
//            let B = prevD ?? (prevPoint - perpNormal * width / 2)
//            let C = curPoint + perpNormal * width / 2
//            let D = curPoint - perpNormal * width / 2
//            
//            prevC = C
//            prevD = D
//            
//            result.append(Vertex(position: A, color: color))
//            result.append(Vertex(position: B, color: color))
//            result.append(Vertex(position: C, color: color))
//            result.append(Vertex(position: B, color: color))
//            result.append(Vertex(position: C, color: color))
//            result.append(Vertex(position: D, color: color))
//            
//            prevPoint = curPoint
//        }
//            while(index < xRange.count)
//        
//        return result
//    }
   
    // MARK: -
    
    func chartAtIndex(index: Int, visible: Bool) {
        visible ? animators[index].show() : animators[index].hide()
        
        if let maxYValue = maxYValueInVisibleRange() {
            animators.forEach { $0.setNewVisibleRange(newStepX: $0.currentXStep(), newMaxY: maxYValue, lowerBound: $0.lowerBound, animated: true) }
        }
    }
    
    func maxYValueInVisibleRange() -> Float? {
        guard let visibleRange = visibleRange, let chart = chart else { return nil }
        
        let intUpper = Int(visibleRange.upperBound)
        let intLower = Int(visibleRange.lowerBound)
        
        if (intUpper - intLower + 1) == chart.xValues.count {
            let indices = animators.indices.filter { animators[$0].isVisible }
            var maxValue: Float = 0
            for i in indices {
                 let max = chart.yValues[i][intLower...intUpper].max() ?? 0
                if max > maxValue {
                    maxValue = max
                }
            }
            self.maxY = maxValue
            
            return maxY
        }
        
        var maxYValue: Float = 0
        for i in chart.yValues.indices where !animators[i].isHidden {
            let max = chart.yValues[i][intLower...intUpper].max()!
            if max > maxYValue {
                maxYValue = max
            }
        }
        self.maxY = maxYValue
        
        return maxYValue
    }
    
    func setVisibleRange(lowerBound: Float, upperBound: Float, animated: Bool) {
        visibleRange = lowerBound...upperBound
        
        let floatXStep = contentWidth / (upperBound - lowerBound)

        xStep = CGFloat(floatXStep)
        
        if let maxYValue = maxYValueInVisibleRange() {
            for animator in animators {
                animator.setNewVisibleRange(newStepX: floatXStep, newMaxY: maxYValue, lowerBound: lowerBound, animated: animated)
            }
        }
    }
    
    func convertToLocalCoordinates(x: CGFloat) -> CGFloat {
        guard let animator = animators.first(where: { $0.isVisible }), let visibleRange = visibleRange else { return x }
        
        let currentStep = CGFloat(animator.currentXStep())
        let d = Float(Int(visibleRange.lowerBound)) - visibleRange.lowerBound
        let convertedX = CGFloat(Int(x / currentStep)) * xStep + CGFloat(d) * currentStep
        
        return convertedX
    }
    
    func index(atX x: CGFloat) -> Int {
        guard let animator = animators.first else { return 0 }
        
        let currentStep = CGFloat(animator.currentXStep())
        return Int(x / currentStep) - Int(CGFloat(animator.currentMove.x) / currentStep)
    }

}

// MARK: - MTKViewDelegate

extension MetalChartRenderer: MTKViewDelegate {
    
    func setSize(size: CGSize) {
        viewportSize.x = Float(size.width)
        viewportSize.y = Float(size.height)
        
        if let chart = chart {
            renderChart(chart: chart)
        }
    }
    
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {}
    
    func draw(in view: MTKView) {
        guard let commandBufer = commandQueue.makeCommandBuffer() else { return }
        
        commandBufer.label = "Command Buffer"

        if let renderPassDescriptor = view.currentRenderPassDescriptor, let renderEncoder = commandBufer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) {
            renderEncoder.label = "Render Encoder"
            renderEncoder.setCullMode(.front)
            renderEncoder.setRenderPipelineState(pipelineState)

            for i in 0..<chartVerticiesBuffer.count where animators[i].isVisible {
                var scaleFactor = animators[i].scaleFactor()
                var moveFactor = animators[i].moveFactor()

                let notVisibleLeftVerticies = max(abs(Int(moveFactor.x / animators[i].currentXStep())) - 1, 0)
                var notVisibleRightVerticies = max(chart!.xValues.count - 5 - Int(contentWidth / animators[i].currentXStep()) - notVisibleLeftVerticies, 0)
                if notVisibleRightVerticies > notVisibleLeftVerticies {
                    notVisibleRightVerticies = 0
                }

                renderEncoder.setVertexBuffer(chartVerticiesBuffer[i], offset: notVisibleLeftVerticies * MemoryLayout<Vertex>.stride, index: 0)

                renderEncoder.setVertexBytes(&viewportSize, length: MemoryLayout.size(ofValue: viewportSize), index: 1)

                renderEncoder.setVertexBytes(&scaleFactor, length: MemoryLayout<float2>.stride, index: 2)

                renderEncoder.setVertexBytes(&moveFactor, length: MemoryLayout<float2>.stride, index: 3)

                renderEncoder.setFragmentBytes(&colors[i], length: MemoryLayout<float4>.stride, index: 0)

                renderEncoder.drawPrimitives(type: self.primitiveType, vertexStart: 0, vertexCount: chartVerticiesBuffer[i].length / MemoryLayout<Vertex>.stride - notVisibleLeftVerticies - notVisibleRightVerticies)
            }

            renderEncoder.endEncoding()

            commandBufer.present(view.currentDrawable!)

            commandBufer.commit()
        }
    }
}

// MARK: - ChartAnimatorDelegate

extension MetalChartRenderer: ChartAnimatorDelegate {
    func chartAnimatordidStartAnimatingChart(_ animator: ChartAnimator) {
        guard dynamicFrameRateEnabled else { return }
        
        metalView?.preferredFramesPerSecond = 60
    }
    
    func chartAnimatordidFinishAnimatingChart(_ animator: ChartAnimator) {
        guard dynamicFrameRateEnabled else { return }
        
        metalView?.preferredFramesPerSecond = 10
    }
}
