//
//  NewChartAnimator.swift
//  MetalChartRenderer
//
//  Created by Алексей Остапенко on 16/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import Foundation
import simd

protocol ChartAnimatorDelegate: AnyObject {
    func chartAnimatordidStartAnimatingChart(_ animator: ChartAnimator)
    func chartAnimatordidFinishAnimatingChart(_ animator: ChartAnimator)
}

class ChartAnimator {
    
    // MARK: - Property
    
    private var fps: Float = 10
    private var targetScale: float2 = [1, 1]
    private(set) var currentScale: float2 = [1, 1]
    private var scaleCount: Int = 0
    private var scaleStep: float2 = [0, 0]
    private var targetMove: float2 = [0, 0]
    private(set) var currentMove: float2 = [0, 0] {
        didSet {
            if currentMove.y < 0 {
                currentMove.y = 0
            }
            if currentMove.y > viewPortHeight {
                currentMove.y = viewPortHeight
            }
            if currentMove.x > 0 {
                currentMove.x = 0
            }
        }
    }
    private var moveCount: Int = 0
    private var moveStep: float2 = [0, 0]
    
    private var originalXStep: Float
    private var originalMaxY: Float
    private var viewPortHeight: Float
    
    private(set) var lowerBound: Float = 0
    private(set) var isHidden = false
    
    var animating: Bool {
        if moveStep == [0, 0] && scaleStep == [0, 0]  {
            return false
        }
        return true
    }
    var isVisible: Bool {
        if isHidden && !animating {
            return false
        }
        else {
           return true
        }
    }
    
    weak var delegate: ChartAnimatorDelegate?
    
    // MARK: - Init
    
    init(xStep: Float, maxY: Float, viewHeight: Float) {
        originalXStep = xStep
        originalMaxY = maxY
        viewPortHeight = viewHeight
    }
    
    // MARK: -
    
    func resetTo(xStep: Float, maxY: Float) {
        originalXStep = xStep
        originalMaxY = maxY
    }
    
    func show() {
        isHidden = false
        currentMove.x = targetMove.x
        moveStep = [0, 0]
        moveCount = 0
        targetMove = [targetMove.x, 0]
    }
    
    func hide() {
        guard !isHidden else { return }
        
        isHidden = true
        currentMove.x = targetMove.x
        moveStep = [0, 0]
        moveCount = 0
        targetMove = [targetMove.x, viewPortHeight]
    }
    
    func setNewVisibleRange(newStepX: Float, newMaxY: Float, lowerBound: Float, animated: Bool) {
        let targetXScaleFactor = newStepX / originalXStep
        let targetYScaleFactor = originalMaxY / (newMaxY == 0 ? originalMaxY : newMaxY)
        let targetXMoveFactor = (0 - lowerBound) * newStepX
        
        targetScale = [targetXScaleFactor, targetYScaleFactor]
        targetMove = [targetXMoveFactor, targetMove.y]
        
        if animated {
            var id: Float = 1
            if targetMove.y < currentMove.y || targetMove.y == currentMove.y {
                id = -1
            }
            let moveDelta: float2 = [abs(currentMove.x) + targetXMoveFactor, id * currentMove.y + targetMove.y]
            moveStep = moveDelta / fps
            moveCount = Int(fps)
            
            let scaleDelta = abs(targetScale) - currentScale
            scaleStep = scaleDelta / fps
            scaleCount = Int(fps)
            
            delegate?.chartAnimatordidStartAnimatingChart(self)
        } else {
            currentScale = targetScale
            currentMove = targetMove
            scaleCount = 0
            moveCount = 0
        }
        
        self.lowerBound = lowerBound
    }
    
    func scaleFactor() -> float2 {
        if scaleCount > 0 {
            scaleCount -= 1
            currentScale += scaleStep
        } else {
            notifyIfNeeded()
            scaleStep = [0, 0]
        }
        
        return currentScale
    }
    
    func moveFactor() -> float2 {
        if moveCount > 0 {
            moveCount -= 1
            currentMove += moveStep
        } else {
            notifyIfNeeded()
            moveStep = [0, 0]
        }
        
        if currentMove.x > 0 {
            print("AGA")
        }
        
        if currentMove.y < 0 {
            print("WTF")
        }
        
        return currentMove
    }
    
    func notifyIfNeeded() {
        if moveCount == 0 && scaleCount == 0 {
            delegate?.chartAnimatordidFinishAnimatingChart(self)
        }
    }
    
    func currentXStep() -> Float {
        return originalXStep * currentScale.x
    }
}
