//
//  Chart.swift
//  TelegramChart
//
//  Created by Алексей Остапенко on 19/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

class Chart {
    var colors: [UIColor]
    var names: [String]
    var xValues: [TimeInterval]
    var yValues: [[Float]]
    var maxYValues: [Float]
    
    enum CodingKeys: String, CodingKey {
        case colors
        case names
        case columns
    }
    
    init(from json: [String: Any]) {
        colors = [UIColor]()
        names = [String]()
        xValues = [TimeInterval]()
        yValues = [[Float]]()
        maxYValues = [Float]()
        
        if let hexColors = json[CodingKeys.colors.rawValue] as? [String: String] {
            for hexColor in hexColors.values {
                let color = UIColor.hexStringToUIColor(hex: hexColor)
                colors.append(color)
            }
        }
        
        if let names = json[CodingKeys.names.rawValue] as? [String: String] {
            for name in names.values {
                self.names.append(name)
            }
        }
        
        if let columns = json[CodingKeys.columns.rawValue] as? [[Any]] {
            for column in columns {
                if (column.first as? String) == "x" {
                    xValues = column[1...].compactMap { $0 as? TimeInterval }
                } else {
                    var values = Array<Float>()
                    values.reserveCapacity(column.count - 2)
                    var maxValue: Float = 0
                    column[1...].forEach { value in
                        guard let value = value as? Float else { return }
                        
                        maxValue = max(value, maxValue)
                        values.append(value)
                    }
                    
                    yValues.append(values)
                    maxYValues.append(maxValue)
                }
            }
        }
    }
}
