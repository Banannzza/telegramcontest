//
//  ChartListViewController.swift
//  TelegramChart
//
//  Created by Алексей Остапенко on 19/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

class ChartListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "SimpleCell"
    
    var charts = [Chart]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        loadCharts()
    }
    
    func loadCharts() {
        DispatchQueue.global().async { [weak self] in
            guard let self = self else { return }

            if let filepath = Bundle.main.path(forResource: "chart_data", ofType: "json") {
                do {
                    let urlFilePath = URL(fileURLWithPath: filepath)
                    let contents = try! Data(contentsOf: urlFilePath)
                    let json = try! JSONSerialization.jsonObject(with: contents, options: []) as! [[String: Any]]
                    
                    var newCharts = [Chart]()
                    
                    for chartJson in json {
                        newCharts.append(Chart(from: chartJson))
                    }
                    
                    DispatchQueue.main.async {
                        self.charts = newCharts
                        if !self.charts.isEmpty {
                            self.tableView.performBatchUpdates({
                                self.tableView.insertRows(at: newCharts.indices.map { IndexPath(row: $0, section: 0) }, with: .fade)
                            }, completion: nil)
                        }
                    }
                   
                }
            }
        }
        
    }
    
    func configureTableView() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
    }
}

extension ChartListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return charts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = "\(indexPath.row + 1) chart"

        return cell
    }
}

extension ChartListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        let vcIdentifier = String(describing: ChartViewController.self)
        let vc = UIStoryboard(name: vcIdentifier, bundle: nil).instantiateViewController(withIdentifier: vcIdentifier) as! ChartViewController
        vc.chart = charts[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
