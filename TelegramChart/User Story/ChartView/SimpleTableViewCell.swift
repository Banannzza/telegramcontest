//
//  SimpleTableViewCell.swift
//  TelegramChart
//
//  Created by Алексей Остапенко on 20/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

class SimpleTableViewCell: UITableViewCell {
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        colorView.layer.cornerRadius = 4
        colorView.layer.masksToBounds = true
    }
    
    func configureWith(color: UIColor, name: String) {
        nameLabel.text = name
        colorView.backgroundColor = color
    }
}
