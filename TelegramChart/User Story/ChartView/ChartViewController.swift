//
//  ChartViewController.swift
//  TelegramChart
//
//  Created by Алексей Остапенко on 19/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

import UIKit

class ChartViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var chartNameLabelContainer: UIView!
    @IBOutlet weak var chartNameLabel: UILabel!
    @IBOutlet weak var chartViewContainer: UIView!
    @IBOutlet weak var sliderViewContainer: UIView!
    @IBOutlet weak var modeButton: UIButton!
    @IBOutlet weak var tableViewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Property
    
    enum Mode {
        case white
        case black
    }
    
    var mode: Mode = .white
    var chart: Chart?
    var sliderView: ChartWithSliders?
    var chartView: ChartView?
    var notVisibleGraph = [Int]()
    
    let darkModeButtonTitle = "Switch to Night Mode"
    let lightModeButtonTitle = "Switch to Day Mode"
    let cellIdentifier = "SimpleCell"
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChartView()
        addSliderView()
        render()
        setColors()
        setupViews()
    }
    
    func setupViews() {
        tableView.tableFooterView = UIView()
        tableView.bounces = false
    }
    
    func render() {
        guard let chart = chart else { return }
        
        sliderView?.renderChart(chart: chart)
        chartView?.renderChart(chart: chart)
    }
    
    func addChartView() {
        let chartView = ChartView(frame: chartViewContainer.frame)
        chartViewContainer.addSubviewWithConstraints(chartView, edgeInset: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20))
        self.chartView = chartView
    }
    
    func addSliderView() {
        let chartSliderView = ChartWithSliders(frame: sliderViewContainer.frame)
        sliderViewContainer.addSubviewWithConstraints(chartSliderView, edgeInset: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20))
        sliderView = chartSliderView
        chartSliderView.delegate = chartView!
    }
    
    // TODO: color constants
    func setColors() {
        switch mode {
        case .white:
            modeButton.setTitle(darkModeButtonTitle, for: .normal)
            chartView?.labelsColor = .black
            navigationController?.navigationBar.tintColor = .black
            navigationController?.navigationBar.barTintColor = UIColor(red: 254 / 255, green: 254 / 255, blue: 254 / 255, alpha: 1)
            chartNameLabelContainer.backgroundColor = UIColor(red: 239 / 255, green: 239 / 255, blue: 244 / 255, alpha: 1)
            chartView?.extraInfoViewColor = UIColor(red: 245 / 255, green: 245 / 255, blue: 245 / 255, alpha: 1)
            chartView?.backgroundColor = UIColor(red: 254 / 255, green: 254 / 255, blue: 254 / 255, alpha: 1)
            chartViewContainer.backgroundColor = UIColor(red: 36 / 255, green: 47 / 255, blue: 62 / 255, alpha: 1)
            chartViewContainer.backgroundColor = UIColor(red: 254 / 255, green: 254 / 255, blue: 254 / 255, alpha: 1)
            tableView.separatorColor = .gray
        case .black:
            modeButton.setTitle(lightModeButtonTitle, for: .normal)
            chartNameLabelContainer.backgroundColor = UIColor(red: 26 / 255, green: 34 / 255, blue: 44 / 255, alpha: 1)
            chartViewContainer.backgroundColor = UIColor(red: 36 / 255, green: 47 / 255, blue: 62 / 255, alpha: 1)
            chartView?.labelsColor = UIColor(red: 91 / 255, green: 104 / 255, blue: 119 / 255, alpha: 1)
            chartView?.linesColor = UIColor(red: 23 / 255, green: 29 / 255, blue: 27 / 255, alpha: 1)
            chartView?.extraInfoViewColor = UIColor(red: 23 / 255, green: 29 / 255, blue: 27 / 255, alpha: 1)
            
            chartNameLabel.textColor = chartView?.labelsColor
            
            navigationController?.navigationBar.tintColor = .white
            navigationController?.navigationBar.barTintColor = chartViewContainer.backgroundColor
            tableView.separatorColor = .black
        }
        chartNameLabel.textColor = chartView?.labelsColor
        modeButton.backgroundColor = chartViewContainer.backgroundColor
        sliderView?.backgroundColor = chartViewContainer.backgroundColor
        sliderViewContainer.backgroundColor = chartViewContainer.backgroundColor
        chartView?.backgroundColor = chartViewContainer.backgroundColor
        tableViewContainer.backgroundColor = chartViewContainer.backgroundColor
        view.backgroundColor = chartNameLabelContainer.backgroundColor
   
        tableView.visibleCells.forEach { cell in
            guard let simpleCell = cell as? SimpleTableViewCell else { return }
            simpleCell.nameLabel.textColor = mode == .black ? .white : .black
        }
    
    }
    
    
    @IBAction func colorModeButtonClick(_ sender: Any) {
        mode = mode == .white ? .black : .white
        UIView.animate(withDuration: 0.3) {
            self.setColors()
        }
    }
    
}

// MARK: - UITableViewDataSource

extension ChartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let chart = chart else { return 0 }
        return chart.names.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let chart = chart else { return UITableViewCell() }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SimpleTableViewCell
        cell.configureWith(color: chart.colors[indexPath.row], name: chart.names[indexPath.row])
        cell.selectionStyle = .none
        cell.nameLabel.textColor = mode == .black ? .white : .black
        cell.backgroundColor = .clear
        cell.accessoryType = notVisibleGraph.contains(indexPath.row) ? .none : .checkmark
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ChartViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        defer {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        let visible = notVisibleGraph.contains(indexPath.row) ? false : true
        if visible {
            notVisibleGraph.append(indexPath.row)
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        } else {
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            if let index = notVisibleGraph.firstIndex(of: indexPath.row) {
                notVisibleGraph.remove(at: index)
            }
        }
        
        chartView?.chartAtIndex(index: indexPath.row, visible: !visible)
        sliderView?.chartRenderer.chartAtIndex(index: indexPath.row, visible: !visible)
    }
}
