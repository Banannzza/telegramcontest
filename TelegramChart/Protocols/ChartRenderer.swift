//
//  ChartRenderer.swift
//  TelegramChart
//
//  Created by Алексей Остапенко on 19/03/2019.
//  Copyright © 2019 Алексей Остапенко. All rights reserved.
//

import UIKit

protocol ChartRenderer {
    func renderChart(chart: Chart)

    var xStep: CGFloat { get }
}


